# imports
import cv2
from os import listdir
from numpy import asarray
from numpy import save
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from PIL import Image
from keras.preprocessing.image import ImageDataGenerator
import matplotlib.pyplot as pyplot
import os
import numpy as np
from keras.applications.vgg16 import VGG16
from os import makedirs
from random import seed
from random import random

#folder = 'images/Green-Tomatoes/'
photos, labels = list(), list()
# enumerate files in the directory

for folder in ["E:/tomatoes/healthy_tomatoes/", "E:/tomatoes/unhealthy_tomatoes/"]:
    for file in listdir(folder):
        # determine class
        print(file)
        output = 0.0
        
        if file.startswith('Healthy'):
            output = 1.0

        # load image
        photo = load_img(folder + file, target_size=(128, 128))
        # convert to numpy array
        photo = img_to_array(photo) / 255
        # store
        photos.append(photo)
        labels.append(output)
        

# convert to a numpy arrays
photos = asarray(photos)
labels = asarray(labels)

print(photos.shape, labels.shape)

# save the reshaped photos
save('healthy-unhealthy-tomatoes-photos.npy', photos)
save('healthy-unhealthy-tomatoes-labels.npy', labels)