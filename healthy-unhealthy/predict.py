from keras.models import load_model
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
import os

 # load and prepare the image
def load_image(filename):
    # load the image
    img = load_img(filename, target_size=(128, 128))
    # convert to array
    img = img_to_array(img)
    # reshape into a single sample with 3 channels
    img = img.reshape(1, 128, 128, 3)
    # center pixel data
    img = img.astype('float32')
    img = img / 255
    #img = img - [123.68, 116.779, 103.939]
    return img

# loading the model
model = load_model('model_v9.h5')

# looping through the predict folder using the loaded model to predict the images
for image in os.listdir('predict'):
    img = load_image(f"predict/{image}")
    result = model.predict(img)
    print(image)
    print(result)
    print('---')