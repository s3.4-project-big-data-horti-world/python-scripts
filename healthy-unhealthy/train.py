import matplotlib.pyplot as plt
import numpy as np
import os

from keras.applications.vgg16 import VGG16
from keras.layers import Flatten
from keras.layers import Dense
from keras.models import Model
from sklearn.model_selection import train_test_split

# function for defining the model
def define_model():
    # load model
    model = VGG16(include_top=False, input_shape=(128, 128, 3))
    # mark loaded layers as not trainable
    for layer in model.layers:
        layer.trainable = False
    # add new classifier layers
    flat1 = Flatten()(model.layers[-1].output)
    class1 = Dense(1500, activation='relu', kernel_initializer='he_uniform')(flat1)
    output = Dense(1, activation='sigmoid')(class1)

    # define new model
    model = Model(inputs=model.inputs, outputs=output)

    model.compile(optimizer='Adam', loss='binary_crossentropy', metrics=['accuracy'])
    
    return model

# loading the numpy arrays
x = np.load('healthy-unhealthy-tomatoes-photos.npy')
y = np.load('healthy-unhealthy-tomatoes-labels.npy')

# splitting the numpy arrays into train and test using a 0.2 ratio
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.2, random_state=2020)

# defining the model
model = define_model()

# fitting the model using 1 epoch with the data
metrics = model.fit(x_train, y_train, batch_size=16, epochs=2, validation_data=(x_test, y_test))

# evaluation of the model using the data
evaluation = model.evaluate(x_test, y_test)

# printing the results
print("Validation Loss, Validation Accuracy")
print(evaluation)

# saving the model
model.save("model.h5")

# printing the metrics
print(metrics.history.keys())

# plotting the results, in this case no line, because of only 1 epoch
plt.plot(metrics.history["accuracy"])
plt.plot(metrics.history["val_accuracy"])
plt.ylabel("Accuracy")
plt.xlabel("Epoch")
plt.legend(["Train", "Val"])
plt.show()

# plotting the results, in this case no line, because of only 1 epoch
plt.plot(metrics.history["loss"])
plt.plot(metrics.history["val_loss"])
plt.ylabel("Loss")
plt.xlabel("Epoch")
plt.legend(["Traing", "Val"])
plt.show()