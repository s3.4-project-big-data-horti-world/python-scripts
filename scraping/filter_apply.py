import cv2
import numpy as np
from skimage.util import random_noise
from PIL import Image, ImageFilter
import os
 
figure_size = 9
outputDirectory = './images/growing_tomatoes/'

# Function for applying mean filter
def apply_mean_filter(image, img_source):
    # New image with 'blur' effect
    new_image = cv2.blur(image,(figure_size, figure_size))

    # Write images to Testing folder
    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_mean.png', new_image)

# Function for applying median filter
def apply_median_filter(image, img_source):
    # New image with 'median' effect
    new_image = cv2.medianBlur(image, figure_size)

     # Write images to Testing folder
    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_plot.png', new_image)
   
# Function for applying unsharp filter
def apply_unsharp_filter(image, img_source):
    # Making unsharp_image
    gaussian_3 = cv2.GaussianBlur(image, (9,9), 10.0)
    unsharp_image = cv2.addWeighted(image, 1.5, gaussian_3, -0.5, 0, image)

    # Writing the image to Testing folder
    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_unsharp.png', unsharp_image)
       
# Function for applying invert filter
def apply_invert_filter(image, img_source):
    # Making and saving invert image
    img_not = cv2.bitwise_not(image)
    
    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_invert.png', img_not)

    
def apply_random_filter(image, img_source):
    #-----Converting image to LAB Color model----------------------------------- 
    lab= cv2.cvtColor(image, cv2.COLOR_BGR2LAB)

    #-----Splitting the LAB image to different channels-------------------------
    l, a, b = cv2.split(lab)

    #-----Applying CLAHE to L-channel-------------------------------------------
    clahe = cv2.createCLAHE(clipLimit=3.0, tileGridSize=(8,8))
    cl = clahe.apply(l)

    #-----Merge the CLAHE enhanced L-channel with the a and b channel-----------
    limg = cv2.merge((cl,a,b))

    #-----Converting image from LAB Color model to RGB model--------------------
    final = cv2.cvtColor(limg, cv2.COLOR_LAB2BGR)

    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_random.png', final)
    
# Function for adjusting gamma to image
def apply_adjust_gamma(image, img_source, gamma=1.0):
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255
            for i in np.arange(0, 256)]).astype("uint8")

    adjusted1 = cv2.LUT(image, table)
    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_adjust-gamma-' + str(gamma) + '.png', adjusted1)

def hsvFilter(image, img_source):
    newImage = cv2.cvtColor(image, cv2.COLOR_BGR2HSV) # convert to HSV
    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_hsvFilter.png', newImage)

def grayFilter(image, img_source):
    grayImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_grayFilter.png', grayImage)

def imageRotateFlip(image, img_source):
    #rotate 90 degrees
    rotatedImg = np.rot90(image)
    cv2.imwrite(str(outputDirectory)+ str(img_source)[:-4] + '_flip.png', rotatedImg)
    
    #flip image
    flippedImg = np.rot90(rotatedImg)
    cv2.imwrite(str(outputDirectory)+ str(img_source)[:-4] + '_rotate.png', flippedImg)

     #mirror image
    flippedImg = np.fliplr(image)
    cv2.imwrite(str(outputDirectory)+ str(img_source)[:-4] + '_mirror.png', flippedImg)

def saltAndPepper(image, img_source):
    noise_img = random_noise(image, mode='s&p',amount=0.3)
    edited_img = np.array(255*noise_img, dtype = 'uint8')

    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_saltAndPepper.png', edited_img)

def gaussion(image, img_source):
    noise_img = random_noise(image, mode='gaussian')
    edited_img = np.array(255*noise_img, dtype = 'uint8')

    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_gaussion.png', edited_img)

def poisson(image, img_source):
    noise_img = random_noise(image, mode='poisson')
    edited_img = np.array(255*noise_img, dtype = 'uint8')

    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_poisson.png', edited_img)

def speckle(image, img_source):
    noise_img = random_noise(image, mode='speckle')    
    edited_img = np.array(255*noise_img, dtype = 'uint8')

    cv2.imwrite(str(outputDirectory) + str(img_source)[:-4] + '_speckle.png', edited_img)    


directory = './images/growing_tomatoes'
maxItems = len(os.listdir(directory))
count = 1

# Loop for applying filters per image
for filename in os.listdir(directory):
    if (filename.endswith('.jpg')) or (filename.endswith('.png')):
        path = directory + '/' + filename
        img_read = cv2.imread(path)
        
        print('Processing image ' + str(count) + ' of ' + str(maxItems))

        # Applying the filters and saving the images
        apply_mean_filter(img_read, filename)
        apply_median_filter(img_read, filename)
        apply_unsharp_filter(img_read, filename)
        apply_invert_filter(img_read, filename)
        apply_random_filter(img_read, filename)
        apply_adjust_gamma(img_read, filename, gamma=0.5)
        apply_adjust_gamma(img_read, filename, gamma=1.5)
        apply_adjust_gamma(img_read, filename, gamma=2.5)
        hsvFilter(img_read, filename)
        grayFilter(img_read, filename)
        saltAndPepper(img_read, filename)
        gaussion(img_read, filename)
        poisson(img_read, filename)
        speckle(img_read, filename)

        count += 1

maxItemsEdtitedImages = len(os.listdir(outputDirectory))
editedCount = 1

#Flip and twist images in outputDirectory
for editedFileName in os.listdir(outputDirectory):
    if (editedFileName.endswith('.jpg')) or (editedFileName.endswith('.png')):
        path = directory + '/' + editedFileName
        img_read = cv2.imread(path)

        print('Processing edited images (flipping and twisting) ' + str(editedCount) + ' of ' + str(maxItemsEdtitedImages))        

        imageRotateFlip(img_read, editedFileName)

        editedCount += 1


print('All images are processed')